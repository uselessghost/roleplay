local Commands = {}

function roleplay.Command( cmd, func )
	Commands[ cmd ] = func
end

function roleplay.Alias( alias, cmd )
	Commands[ alias ] = Commands[ cmd ]
end

function roleplay.RemoveCommand( cmd )
	Commands[ cmd ] = nil
end

function roleplay.CommandExists( cmd )
	return Commands[ cmd ] ~= nil
end

function roleplay.RunCommand( pl, cmd, args )
	if Commands[ cmd ] then return Commands[ cmd ]( pl, args ) end
end

function roleplay.ParseCommands( pl, text, team )
	local match = string.match( text, "/(.+)" )
	if not match then return false end

	local args = string.Explode( " ", match )
	local cmd = table.remove( args, 1 )

	if not roleplay.CommandExists( cmd ) then return end
	
	roleplay.RunCommand( pl, cmd, args )
	
	return true
end

concommand.Add( "rp", function( pl, cmd, args )
	if not IsValid( pl ) then return end

	local cmd = table.remove( args, 1 )
	if not roleplay.CommandExists( cmd ) then return end

	roleplay.RunCommand( pl, cmd, args )
end )