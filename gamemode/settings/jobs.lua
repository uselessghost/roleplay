roleplay.Job( "citizen", {
	Name = "Citizen",
	Description = "A dime-a-dozen average citizen wandering the city. Low pay but no obligations.",
	Color = Color( 25, 175, 25 ),
	Salary = 45,
	Models = {
		"models/player/Group01/Female_01.mdl",
		"models/player/Group01/Female_02.mdl",
		"models/player/Group01/Female_03.mdl",
		"models/player/Group01/Female_04.mdl",
		"models/player/Group01/Female_06.mdl",
		"models/player/group01/male_01.mdl",
		"models/player/Group01/Male_02.mdl",
		"models/player/Group01/male_03.mdl",
		"models/player/Group01/Male_04.mdl",
		"models/player/Group01/Male_05.mdl",
		"models/player/Group01/Male_06.mdl",
		"models/player/Group01/Male_07.mdl",
		"models/player/Group01/Male_08.mdl",
		"models/player/Group01/Male_09.mdl"
    },
} )

roleplay.Job( "cop", {
	Name = "Police",
	Description = "A protector of the city who enforces the mayor's laws. Follows the police chief.",
	Color = Color( 50, 50, 200 ),
	Salary = 65,
	Vote = true,
	Models = { "models/player/police.mdl", "models/player/police_fem.mdl" },
	Weapons = { "lite_glock" }
} )

roleplay.Job( "chief", {
	Name = "Police Chief",
	Description = "The proud leader of the police force. Only one can be active at a time.",
	Color = Color( 100, 100, 255 ),
	Salary = 75,
	Maximum = 1,
	ParentJob = "cop",
	Models = { "models/player/combine_soldier_prisonguard.mdl" },
	Weapons = { "lite_deagle" }
} )

roleplay.Job( "mayor", {
	Name = "Mayor",
	Description = "The leader of the municipal goverment. Creates laws to keep citizens in check.",
	Color = Color( 200, 50, 50 ),
	Salary = 80,
	Maximum = 1,
	Vote = true,
	Models = { "models/player/breen.mdl" },
} )

roleplay.Job( "gangster", {
	Name = "Gangster",
	Description = "A lowly gang member who follows the gang leader's orders.",
	Color = Color( 100, 100, 100 ),
	Salary = 35,
	Models = {
		"models/player/Group03/Female_01.mdl",
		"models/player/Group03/Female_02.mdl",
		"models/player/Group03/Female_03.mdl",
		"models/player/Group03/Female_04.mdl",
		"models/player/Group03/Female_06.mdl",
		"models/player/group03/male_01.mdl",
		"models/player/Group03/Male_02.mdl",
		"models/player/Group03/male_03.mdl",
		"models/player/Group03/Male_04.mdl",
		"models/player/Group03/Male_05.mdl",
		"models/player/Group03/Male_06.mdl",
		"models/player/Group03/Male_07.mdl",
		"models/player/Group03/Male_08.mdl",
		"models/player/Group03/Male_09.mdl"
    }
} )

roleplay.Job( "mobboss", {
	Name = "Mob Boss",
	Description = "Leader of the gang. Coordinates high profile robberies.",
	Color = Color( 20, 20, 20 ),
	Salary = 55,
	Vote = true,
	Models = { "models/player/gman_high.mdl" },
} )

roleplay.Job( "dealer", {
	Name = "Gun Dealer",
	Description = "A licensed gun dealer. Provides armaments for others via shipments.",
	Color = Color( 200, 200, 25 ),
	Salary = 55,
	Models = { "models/player/monk.mdl" },
} )

roleplay.Job( "medic", {
	Name = "Medic",
	Description = "A paramedic responsible for the well being of others. Can heal people using their medkit.",
	Color = Color( 255, 100, 150 ),
	Salary = 55,
	Models = { "models/player/kleiner.mdl" },
	Weapons = { "weapon_medkit" },
} )

roleplay.Job( "hobo", {
	Name = "Hobo",
	Description = "A jobless bum. Beg for money and make homes on the street.",
	Color = Color( 100, 20, 100 ),
	Salary = 55,
	Models = { "models/player/corpse1.mdl" },
} )