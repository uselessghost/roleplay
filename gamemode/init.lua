roleplay = {}

include( "sv_commands.lua" )
include( "sv_chat.lua" )

include( "shared.lua" )

AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "cl_skin.lua" )
AddCSLuaFile( "cl_hud.lua" )

AddCSLuaFile( "shared.lua" )
AddCSLuaFile( "player.lua" )
AddCSLuaFile( "money.lua" )
AddCSLuaFile( "store.lua" )
AddCSLuaFile( "doors.lua" )
AddCSLuaFile( "jobs.lua" )
AddCSLuaFile( "votes.lua" )

AddCSLuaFile( "settings/buyables.lua" )
AddCSLuaFile( "settings/jobs.lua" )

AddCSLuaFile( "vgui/RoleplayVote.lua" )
AddCSLuaFile( "vgui/RoleplayMenu.lua" )
AddCSLuaFile( "vgui/RoleplayJobs.lua" )
AddCSLuaFile( "vgui/RoleplayStore.lua" )
AddCSLuaFile( "vgui/RoleplayOptions.lua" )
