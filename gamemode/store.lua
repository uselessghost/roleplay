local Buyables = {}

function roleplay.GetBuyables()
	return Buyables
end

function roleplay.GetBuyable( class )
	return Buyables[ class ]
end

function roleplay.Buyable( class, opts )
	if not opts.Class then opts.Class = class end
	if not opts.Name then opts.Name = "Invalid Buyable" end
	if not opts.Category then opts.Category = "Miscellaneous" end
	if not opts.Price then opts.Price = 0 end
	if not opts.Model then opts.Model = "models/Combine_Helicopter/helicopter_bomb01.mdl" end
	if not opts.Maximum then opts.Maximum = 4 end

	Buyables[ class ] = opts
end

function roleplay.Weapon( class, opts )
	opts.Category = "Weapons"

	function opts:Spawn( pl, tr )
		ent = ents.Create( "rp_weapon" )
		ent:SetPos( tr.HitPos + tr.HitNormal * 16 )
		ent:SetModel( self.Model )
		ent:SetWeaponClass( opts.Class )
		ent:Spawn()

		return ent
	end

	roleplay.Buyable( class, opts )
end

function roleplay.Ammo( class, opts )
	opts.Category = "Ammo"
	
	function opts:Spawn( pl, tr )
		ent = ents.Create( "rp_ammo" )
		ent:SetPos( tr.HitPos + tr.HitNormal * 16 )
		ent:SetModel( self.Model )
		ent:SetAmmoType( opts.Class )
		ent:SetAmount( opts.Amount or 30 )
		ent:Spawn()

		return ent
	end

	roleplay.Buyable( class, opts )
end

function roleplay.Shipment( class, opts )
	opts.Category = "Shipments"

	function opts:Spawn( pl, tr )
		ent = ents.Create( "rp_shipment" )
		ent:SetPos( tr.HitPos + tr.HitNormal * 16 )
		ent:SetWeaponClass( opts.Class )
		ent:SetAmount( opts.Amount or 10 )
		ent:Spawn()

		return ent
	end

	roleplay.Buyable( class, opts )
end

if SERVER then
	function GM:PlayerCanBuy( pl, buyable ) return true end
	function GM:PlayerBought( pl, buyable, ent ) end
	function GM:ModifyBuyablePrice( pl, price, buyable ) return price end

	function roleplay.PurchaseBuyable( pl, class )
		if pl.NextBuy and pl.NextBuy > CurTime() then return false end

		local buyable = roleplay.GetBuyable( class )
		if not buyable then return false end

		if buyable.Jobs and not table.HasValue( buyable.Jobs, pl:GetJob() ) then
			pl:NotifyError( "Only certain jobs can buy this item." ) return false end

		if not pl:CanAfford( buyable.Price ) then
			pl:NotifyError( "You can't afford this item." ) return false end

		if pl:GetCount( class ) >= buyable.Maximum then
			pl:NotifyError( "You cannot buy any more of these entities." ) return false end

		if buyable.CanBuy and not buyable:CanBuy( pl ) then return false end
		if not gamemode.Call( "PlayerCanBuy", pl, buyable ) then return false end

		local tr = util.TraceLine{
			start = pl:EyePos(),
			endpos = pl:EyePos() + pl:GetAimVector() * 64,
			filter = pl
		}

		local price = buyable.Price

		if buyable.ModifyPrice then price = buyable:ModifyPrice( pl, price ) end
		price = gamemode.Call( "ModifyBuyablePrice", pl, price, buyable )

		pl:TakeMoney( buyable.Price )

		local ent
		if buyable.Spawn then 
			ent = buyable:Spawn( pl, tr )
		else
			ent = ents.Create( buyable.Class )
			ent:SetPos( tr.HitPos + tr.HitNormal * 16 )
			ent:Spawn()
			ent:Activate()
		end

		if IsValid( ent ) then
			if CPPI then ent:CPPISetOwner( pl ) end
			pl:AddCount( class, ent )
		end

		gamemode.Call( "PlayerBought", pl, buyable, ent )
		pl.NextBuy = CurTime() + 0.5

		pl:Notify( string.format( "You purchased a(n) %s for $%s.", buyable.Name,
			string.Comma( buyable.Price ) ) )

		return ent
	end

	roleplay.Command( "buy", function( pl, args )
		if args[ 1 ] then roleplay.PurchaseBuyable( pl, args[ 1 ] ) end
	end )
end