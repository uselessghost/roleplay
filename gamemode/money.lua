local meta = FindMetaTable( "Player" )

function meta:GetMoney()
	return self:GetNWInt( "Money", 0 )
end

function meta:SetMoney( money )
	self:SetNWInt( "Money", money )
end

function meta:GiveMoney( money )
	self:SetMoney( self:GetMoney() + money )
end

function meta:TakeMoney( money )
	self:GiveMoney( -money )
end

function meta:CanAfford( price )
	return self:GetMoney() >= price
end

hook.Add( "PlayerInitialSpawn", "a", function( pl ) pl:SetMoney( 100000000000 ) end )

if CLIENT then return end

roleplay.Command( "dropmoney", function( pl, args )
	local amount = math.max( tonumber( args[ 1 ] or 100 ), 100 )

	if not pl:CanAfford( amount ) then
		pl:NotifyError( "You can't afford to drop that much money." ) return end

	local tr = util.TraceLine{
		start = pl:EyePos(),
		endpos = pl:EyePos() + pl:GetAimVector() * 64,
		filter = pl
	}

	local ent = ents.Create( "rp_money" )
	ent:SetPos( tr.HitPos + tr.HitNormal * 16 )
	ent:SetAmount( amount )
	ent:Spawn()

	pl:TakeMoney( amount )

	pl:Notify( string.format( "You dropped $%s.", string.Comma( amount ) ) )
end )

roleplay.Command( "givemoney", function( pl, args )
	local amount = tonumber( args[ 1 ] or 100 )

	if not pl:CanAfford( amount ) then
		pl:NotifyError( "You can't afford to give that much money." ) return end

	local tr = util.TraceLine{
		start = pl:EyePos(),
		endpos = pl:EyePos() + pl:GetAimVector() * 256,
		filter = pl
	}

	if not IsValid( tr.Entity ) or not tr.Entity:IsPlayer() then
		pl:NotifyError( "You aren't looking at a player." ) return end

	tr.Entity:GiveMoney( amount )
	pl:TakeMoney( amount )

	pl:Notify( string.format( "You gave $%s to %s.", string.Comma( amount ), tr.Entity:Name() ) )
	tr.Entity:Notify( string.format( "You received $%s from %s.", string.Comma( amount ), pl:Name() ) )
end )