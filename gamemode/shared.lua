AddCSLuaFile()

GM.Name = "Roleplay"
GM.Author = "Olivia (UselessGhost)"
GM.Email = "N/A"
GM.Website = "https://uselessghost.me/"

DeriveGamemode( "sandbox" )

include( "classes/player_roleplay.lua" )

include( "money.lua" )
include( "store.lua" )
include( "doors.lua" )
include( "jobs.lua" )
include( "player.lua" )
include( "votes.lua" )

include( "settings/buyables.lua" )
include( "settings/jobs.lua" )
