local CLASS = {}

CLASS.WalkSpeed = 150
CLASS.RunSpeed = 300

CLASS.MaxHealth = 100
CLASS.StartHealth = 100
CLASS.StartArmor = 0

function CLASS:SetModel()
	local j = roleplay.GetJob( self.Player:GetJob() )

	if j and j.Models then
		self.Player:SetModel( table.Random( j.Models ) )
		return
	end

	self.Player:SetModel( "models/player/kleiner.mdl" )
end

function CLASS:Loadout()
	self.Player:StripWeapons()
	self.Player:RemoveAllAmmo()

	self.Player:GiveAmmo( 64, "pistol", true )
	self.Player:GiveAmmo( 32, "smg1", true )
	self.Player:GiveAmmo( 2, "grenade", true )
	self.Player:GiveAmmo( 16, "buckshot", true )
	self.Player:GiveAmmo( 16, "357", true )
	self.Player:GiveAmmo( 8, "xbowbolt", true )
	self.Player:GiveAmmo( 24, "ar2", true )
	
	self.Player:Give( "gmod_tool" )
	self.Player:Give( "gmod_camera" )
	self.Player:Give( "weapon_physgun" )
	self.Player:Give( "rp_keys" )

	local j = roleplay.GetJob( self.Player:GetJob() )

	if j and j.Weapons then
		for k, v in ipairs( j.Weapons ) do
			self.Player:Give( v )
		end
	end

	if j and j.Ammo then
		for k, v in pairs( j.Ammo ) do
			self.Player:GiveAmmo( v, k, true )
		end
	end

	if j and j.Loadout then j:Loadout( self.Player ) end

	self.Player:SwitchToDefaultWeapon()
end

player_manager.RegisterClass( "player_roleplay", CLASS, "player_sandbox" )