if SERVER then
	local Votes = {}

	util.AddNetworkString( "StartVote" )
	function roleplay.StartVote( desc, timeout, success, fail )
		local vote = {}

		vote.Description = desc
		vote.Timeout = timeout
		vote.Success = success
		vote.Fail = fail
		vote.Voters = {}

		local id = table.insert( Votes, vote )

		net.Start( "StartVote" )
		 	net.WriteUInt( id, 8 )
			net.WriteString( desc )
		net.Broadcast()

		timer.Create( "Vote." .. id, timeout, 1, function()
			roleplay.FinishVote( id )
		end )

		return id
	end

	util.AddNetworkString( "StopVote" )
	function roleplay.FinishVote( id )
		local vote = Votes[ id ]
		if not vote then return end

		timer.Destroy( "Vote." .. id )

		local voters = 0
		local positive = 0

		for k, v in pairs( vote.Voters ) do
			if IsValid( k ) then
				voters = voters + 1

				if v then
					positive = positive + 1
				end
			end
		end

		if positive / voters >= 0.5 then
			if vote.Success then vote:Success() end
		else
			if vote.Fail then vote:Fail() end
		end

		Votes[ id ] = nil

		net.Start( "StopVote" )
		 	net.WriteUInt( id, 8 )
		net.Broadcast()
	end

	function roleplay.CancelVote( id )
	end

	util.AddNetworkString( "CastVote" )
	net.Receive( "CastVote", function( len, pl )
		local id = net.ReadUInt( 8 )
		local choice = net.ReadBool()

		local vote = Votes[ id ]
		if not vote or vote.Voters[ pl ] then return end

		vote.Voters[ pl ] = choice

		local n = 0
		
		for k, v in pairs( vote.Voters ) do
			if IsValid( k ) then n = n + 1 end
		end

		if n == #player.GetAll() then roleplay.FinishVote( id ) end
	end )
else
	local Panels = {}

	net.Receive( "StartVote", function()
	 	local id = net.ReadUInt( 8 )
		local desc = net.ReadString()

		local p = vgui.Create( "RoleplayVote" )
		p:SetPos( 0, ScrH() * 0.25 + id * 25 )
		p:SetVoteID( id )
		p:SetDescription( desc )

		Panels[ id ] = p

		surface.PlaySound( "plats/elevbell1.wav" )
	end )

	net.Receive( "StopVote", function()
	 	local id = net.ReadUInt( 8 )

		if IsValid( Panels[ id ] ) then
			Panels[ id ]:Remove()
		end

		Panels[ id ] = nil
	end )
end