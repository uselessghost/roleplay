local GradientUp = Material( "gui/gradient_up" )
local GradientDown = Material( "gui/gradient_down" )
local MoneyIcon = Material( "icon16/money.png" )
local MoneyAddIcon = Material( "icon16/money_add.png" )
local UserIcon = Material( "icon16/user_suit.png" )

function GM:DrawRect( x, y, w, h )
	surface.SetDrawColor( Color( 0, 0, 0, 200 ) )
	surface.DrawRect( x, y, w, h )
end

function GM:DrawProgressBar( x, y, w, h, filled, color, text, text_font, alignment )
	self:DrawRect( x, y, w, h )

	filled = math.Clamp( filled, 0, 1 )

	local base_color = Color( color.r / 2, color.g / 2, color.b / 2, color.a )
	surface.SetDrawColor( base_color )
	surface.DrawRect( x, y, w * filled, h )
	
	surface.SetMaterial( GradientDown )
	surface.SetDrawColor( color )
	surface.DrawTexturedRect( x, y, w * filled, h )
	
	surface.SetDrawColor( Color( 0, 0, 0, 200 ) )
	surface.DrawOutlinedRect( x, y, w * filled, h )

	if text then
		draw.SimpleTextOutlined( text, text_font or "DermaLarge", x + w / 2, y + h / 2, color_white,
			TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, color_black )
	end
end

function GM:DrawIcon( x, y, icon )
	surface.SetMaterial( icon )
	surface.SetDrawColor( Color( 255, 255, 255 ) )
	surface.DrawTexturedRect( x, y, 16, 16 )
end

local ShouldDraw = GetConVar( "cl_drawhud" )
local Health, Armor = 0, 0
function GM:DrawHUD()
	if not ShouldDraw:GetBool() then return end

	local w, h = 400, 64
	local x, y = 10, ScrH() - h - 10
	local pl = LocalPlayer()
	
	self:DrawRect( x, y, w, h )

	Health = Lerp( FrameTime() * 4, Health, pl:Health() )
	Armor = Lerp( FrameTime() * 4, Armor, pl:Armor() )
	
	local health_mul = math.Round( math.Clamp( Health / pl:GetMaxHealth(), 0, 1 ), 3 )
	local armor_mul = math.Round( math.Clamp( Armor / pl:GetMaxHealth(), 0, 1 ), 3 )

	local col = Color( 0, 0, 0 )
	col.r = Lerp( health_mul, 255, 0 )
	col.g = Lerp( health_mul, 0, 255 )
	col.b = 0
	
	local text = tostring( math.Round( Health ) )

	self:DrawProgressBar( x + 5, y + h - 40 - 18, w - 10, 40, health_mul, col, text )
	self:DrawProgressBar( x + 5, y + h - 5 - 8, w - 10, 8, armor_mul, Color( 0, 0, 200 ) )

	local h = 26
	local y = y - 5 - h

	self:DrawRect( x, y, w, h )

	self:DrawIcon( x + 5, y + 5, MoneyIcon )
	draw.SimpleTextOutlined( "$" .. string.Comma( pl:GetMoney() ), "DermaDefaultBold", x + 5 + 16 + 5, y + 6,
		color_white, TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP, 1, color_black )

	self:DrawIcon( x + w - 16 - 5, y + 5, MoneyAddIcon )
	draw.SimpleTextOutlined( "$" .. string.Comma( pl:GetSalary() ) .. "/hr", "DermaDefaultBold", x + w - 5 - 16 - 5, y + 6,
		color_white, TEXT_ALIGN_RIGHT, TEXT_ALIGN_TOP, 1, color_black )

	local y = y - 5 - h

	self:DrawRect( x, y, w, h )

	self:DrawIcon( x + 4, y + 4, UserIcon )
	draw.SimpleTextOutlined( pl:GetJobTitle(), "DermaDefaultBold", x + 26, y + 6,
		color_white, TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP, 1, color_black )
end

function GM:HUDPaint()
	self.BaseClass:HUDPaint()
	
	self:DrawHUD()
	self:DrawDoorInfo()
end

function GM:HUDShouldDraw( el )
	return not ( el == "CHudHealth" or el == "CHudBattery" )
end	