local PANEL = {}

function PANEL:Init()
	self:SetTitle( "Menu" )
	self:SetSkin( "roleplay" )

	self.PropertySheet = vgui.Create( "DPropertySheet", self )
	self.PropertySheet:Dock( FILL )

	self.PropertySheet:AddSheet( "Jobs", vgui.Create( "RoleplayJobs" ), "icon16/user_suit.png" )
	self.PropertySheet:AddSheet( "Store", vgui.Create( "RoleplayStore" ), "icon16/cart.png" )
	self.PropertySheet:AddSheet( "Options", vgui.Create( "RoleplayOptions" ), "icon16/cog.png" )
end

vgui.Register( "RoleplayMenu", PANEL, "DFrame" )