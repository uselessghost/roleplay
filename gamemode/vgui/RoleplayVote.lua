local PANEL = {}

AccessorFunc( PANEL, "VoteID", "VoteID", FORCE_NUMBER )

function PANEL:Init()
	self:SetSize( 400, 24 )
	self:DockPadding( 5, 4, 5, 4 )

	self.Icon = vgui.Create( "DImage", self )
	self.Icon:Dock( LEFT )
	self.Icon:SetSize( 16, 16 )
	self.Icon:SetImage( "icon16/help.png" )	

	self.Label = vgui.Create( "DLabel", self )
	self.Label:Dock( FILL )
	self.Label:DockMargin( 5, 0, 5, 0 )
	self.Label:SetBright( true )

	self.VoteNo = vgui.Create( "DImageButton", self )
	self.VoteNo:Dock( RIGHT ) 
	self.VoteNo:SetSize( 16, 16 )
	self.VoteNo:SetImage( "icon16/cross.png" )

	function self.VoteNo.DoClick()
		self:Vote( false )
	end

	self.VoteYes = vgui.Create( "DImageButton", self )
	self.VoteYes:Dock( RIGHT ) 
	self.VoteYes:DockMargin( 0, 0, 5, 0 )
	self.VoteYes:SetSize( 16, 16 )
	self.VoteYes:SetImage( "icon16/tick.png" )

	function self.VoteYes.DoClick()
		self:Vote( true )
	end
end

function PANEL:Paint( w, h )
	surface.SetDrawColor( Color( 0, 0, 0, 200 ) )
	surface.DrawRect( 0, 0, w, h )
end

function PANEL:Vote( vote )
	net.Start( "CastVote" )
		net.WriteUInt( self:GetVoteID(), 8 )
		net.WriteBool( vote )
	net.SendToServer()

	self:Remove()
end

function PANEL:GetDescription()
	return self.Label:GetText()
end

function PANEL:SetDescription( desc )
	self.Label:SetText( desc )
end

vgui.Register( "RoleplayVote", PANEL )