local meta = FindMetaTable( "Player" )

function meta:GetRespawnTime()
	return self:GetNW2Float( "RespawnTime", 0 )
end

function meta:SetRespawnTime( time )
	self:SetNW2Float( "RespawnTime", time )
end

function roleplay.FindPlayer( name )
	if not name or name == "" then return end

	local pl = player.GetBySteamID( name )
	if pl then return pl end

	for k, v in ipairs( player.GetAll() ) do
		if v:Name() == name then return v end
	end

	for k, v in ipairs( player.GetAll() ) do
		if string.find( string.lower( v:Name() ), string.lower( name ) ) then return v end
	end
end

local NoClip = GetConVar( "sbox_noclip" )
function GM:PlayerNoClip( pl, enable )
	return NoClip:GetBool() or pl:IsAdmin()
end

if SERVER then
	util.AddNetworkString( "Notification" )
	function meta:Notify( text, icon, duration, sound )
		net.Start( "Notification" )
			net.WriteString( text )
			net.WriteUInt( icon or 0, 8 )
			net.WriteFloat( duration or 8 )
			net.WriteString( sound or "buttons/lightswitch2.wav" )
		net.Send( self )
	end

	function roleplay.Notify( text, icon, duration, sound )
		for _, pl in ipairs( player.GetAll() ) do
			pl:Notify( text, icon, duration, sound )
		end
	end

	function meta:NotifyError( text, duration )
		self:Notify( text, 1, duration, "buttons/button10.wav" )
	end

	util.AddNetworkString( "Message" )
	function meta:Message( ... )
		local args = { ... }

		net.Start( "Message" )
			net.WriteUInt( #args, 16 )

			for k, v in ipairs( args ) do
				net.WriteType( v )
			end
		net.Send( self )
	end

	function roleplay.Message( ... )
		for _, pl in ipairs( player.GetAll() ) do
			pl:Message( ... )
		end
	end

	local DefaultJob = GetConVar( "rp_defaultjob" )
	function GM:PlayerInitialSpawn( pl )
		pl:SetJob( DefaultJob:GetString(), true )
	end

	function GM:PlayerSpawn( pl )
		player_manager.SetPlayerClass( pl, "player_roleplay" )

		pl:UnSpectate()
		pl:SetupHands()

		player_manager.OnPlayerSpawn( pl )
		player_manager.RunClass( pl, "Spawn" )

		hook.Call( "PlayerLoadout", self, pl )
		hook.Call( "PlayerSetModel", self, pl )		
	end

	--[[
	local PropCost = CreateConVar( "bw_prop_cost", 10, FCVAR_NOTIFY )
	function GM:PlayerSpawnProp( pl, model )
		return pl:CanAfford( PropCost:GetInt() ) and self.BaseClass:PlayerSpawnProp( pl, model )
	end

	function GM:PlayerSpawnedProp( pl, model, ent )
		self.BaseClass:PlayerSpawnedProp( pl, model, ent )

		pl:Notify( string.format( "You paid $%s to spawn a prop.", string.Comma( PropCost:GetInt() ) ) )
		pl:TakeMoney( PropCost:GetInt() )

		ent:SetCreator( pl )
	end
	]]

	local ChatRange = GetConVar( "rp_chatrange" )
	function GM:PlayerSay( pl, text, team )
		if roleplay.ParseCommands( pl, text, team ) then return "" end

		pl:LocalChat( text, ChatRange:GetFloat() )
		return ""
	end

	function GM:PlayerSpawnSENT( pl, ent_class )
		return pl:IsAdmin() and self.BaseClass:PlayerSpawnSENT( pl, model )
	end

	function GM:PlayerSpawnSWEP( pl, wep_class )
		return pl:IsAdmin() and self.BaseClass:PlayerSpawnSWEP( pl, model )
	end

	function GM:PlayerGiveSWEP( pl, wep_class )
		return pl:IsAdmin() and self.BaseClass:PlayerGiveSWEP( pl, model )
	end

	function GM:PlayerSpawnVehicle( pl, model )
		return pl:IsAdmin() and self.BaseClass:PlayerSpawnVehicle( pl, model )
	end

	function GM:ShowSpare2( pl )
		pl:ConCommand( "rp_menu" )
	end

	roleplay.Command( "dropweapon", function( pl, args )
		local wep = pl:GetActiveWeapon()
		if not IsValid( wep ) then return end

		local ent = ents.Create( "rp_weapon" )
		ent:SetPos( pl:EyePos() + pl:GetAimVector() * 16 )
		ent:SetModel( wep:GetModel() )
		ent:SetWeaponClass( wep:GetClass() )
		ent:Spawn()

		pl:StripWeapon( wep:GetClass() )
	end )
else
	net.Receive( "Notification", function()
		local text = net.ReadString()
		local icon = net.ReadUInt( 8 )
		local duration = net.ReadFloat()
		local sound = net.ReadString()

		notification.AddLegacy( text, icon, duration )
		if sound ~= "" then surface.PlaySound( sound ) end

		MsgC( Color( 100, 200, 255 ), "Roleplay: ", color_white, text, "\n" )
	end )

	net.Receive( "Message", function()
		local args = {}

		for i = 1, net.ReadUInt( 16 ) do
			table.insert( args, net.ReadType() )
		end

		chat.AddText( color_white, unpack( args ) )
	end )
end