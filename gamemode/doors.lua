local meta = FindMetaTable( "Player" )

function meta:OwnsDoor( door )
	if not door:IsDoor() then return end

	if door:GetDoorOwner() == self then return true end

	local j = roleplay.GetJob( self:GetJob() )
	if j and j.DoorGroup and door:GetDoorGroup() == j.DoorGroup then return true end

	if door.Owners and door.Owners[ self ] then return true end

	return false
end

local meta = FindMetaTable( "Entity" )

function meta:IsDoor()
	if not IsValid( self ) then return false end

	return self:GetClass() == "func_door" or
		self:GetClass() == "func_door_rotating" or
		self:GetClass() == "prop_door_rotating"
end

function meta:GetDoorUnownable()
	return self:GetNWBool( "DoorUnownable", false )
end

function meta:GetDoorGroup()
	return self:GetNWString( "DoorGroup" )
end

function meta:GetDoorOwner()
	return self:GetNWEntity( "DoorOwner" )
end

function meta:GetDoorLocked()
	return self:GetNWBool( "DoorLocked", false )
end

function meta:GetDoorTitle()
	return self:GetNWString( "DoorTitle", "" )
end

function meta:GetDoorOwners()
	if not door.Owners then
		door.Owners = {}
	end

	return door.Owners
end

function meta:IsOwned()
	if IsValid( self:GetDoorOwner() ) then return true end
	if self:GetDoorGroup() ~= "" then return true end

	return false
end

if SERVER then
	local DoorPrice = CreateConVar( "rp_doors_price", 50, FCVAR_ARCHIVE )
	local DoorSellPrice = CreateConVar( "rp_doors_sellprice", 40, FCVAR_ARCHIVE )

	function meta:SetDoorOwner( owner )
		self:SetNWEntity( "DoorOwner", owner )
	end

	function meta:SetDoorUnownable( unownable )
		self:SetNWBool( "DoorUnownable", unownable )
		self:SetDoorOwner( NULL )
	end

	function meta:SetDoorGroup( group )
		self:SetNWString( "DoorGroup", group )
	end

	function meta:SetDoorLocked( l )
		if l then
			self:Fire( "Lock" )
		else
			self:Fire( "UnLock" )
		end

		self:SetNWBool( "DoorLocked", l )
	end

	function meta:SetDoorTitle( title )
		self:SetNWString( "DoorTitle", title )
	end

	util.AddNetworkString( "DoorOwners" )
	function meta:SyncDoorOwners()
		net.Start( "DoorOwners" )
			net.WriteEntity( self )
			net.WriteUInt( #self.Owners, 8 )

			for k, v in pairs( self.Owners ) do
				net.WriteEntity( k )
			end
		net.Broadcast()
	end

	function meta:AddDoorOwner( pl )
		if not self.Owners then
			self.Owners = {}
		end

		self.Owners[ pl ] = true

		self:SyncDoorOwners()
	end

	function meta:RemoveDoorOwner( pl )
		if not self.Owners then return end

		self.Owners[ pl ] = nil

		self:SyncDoorOwners()
	end

	local meta = FindMetaTable( "Player" )

	function meta:BuyDoor( door )
		if not door:IsDoor() then return false end

		if door:GetDoorUnownable() then
			self:NotifyError( "This door is unownable!" ) return false end

		if door:IsOwned() then
			self:NotifyError( "This door is already owned." ) return false end

		if not self:CanAfford( DoorPrice:GetInt() ) then
			self:NotifyError( "You can't afford this door!" ) return false end

		if not gamemode.Call( "PlayerCanBuyDoor", self, door ) then return false end

		local price = gamemode.Call( "ModifyDoorPrice", door, DoorPrice:GetInt() )

		self:TakeMoney( DoorPrice:GetInt() )

		door:SetDoorOwner( self )
		self:SetDoorLocked( false )

		gamemode.Call( "PlayerBoughtDoor", self, door )

		self:Notify( string.format( "You bought a door for $%s.", string.Comma( price ) ) )

		return true
	end

	function meta:SellDoor( door )
		if not door:IsDoor() then return false end

		if door:GetDoorOwner() ~= self then
			self:NotifyError( "This door is not owned by you." ) return false end

		if not gamemode.Call( "PlayerCanSellDoor", self, door ) then return false end

		self:GiveMoney( DoorSellPrice:GetInt() )

		door:SetDoorOwner( NULL )

		gamemode.Call( "PlayerSellDoor", self, door )

		self:Notify( string.format( "You sold a door for $%s.", string.Comma( DoorSellPrice:GetInt() ) ) )

		return true
	end

	function GM:ShowTeam( pl )
		pl:SelectWeapon( "rp_keys" )
	end

	function GM:PlayerCanBuyDoor( pl, door ) return true end
	function GM:PlayerBoughtDoor( pl, door ) end
	function GM:PlayerCanSellDoor( pl, door ) return true end
	function GM:PlayerSoldDoor( pl, door ) end
	function GM:PlayerCanLockDoor( pl, door ) return true end
	function GM:PlayerLockDoor( pl, door ) end
	function GM:PlayerCanUnlockDoor( pl, door ) return true end
	function GM:PlayerUnlockDoor( pl, door ) end
	function GM:ModifyDoorPrice( door, price ) return price end
	
	roleplay.Command( "buydoor", function( pl, args )
		local tr = util.TraceLine{
			start = pl:EyePos(),
			endpos = pl:EyePos() + pl:GetAimVector() * 100,
			filter = pl
		}

		local door = tr.Entity
		if not door:IsDoor() then return end

		pl:BuyDoor( door )
	end )

	roleplay.Command( "selldoor", function( pl, args )
		local tr = util.TraceLine{
			start = pl:EyePos(),
			endpos = pl:EyePos() + pl:GetAimVector() * 100,
			filter = pl
		}

		local door = tr.Entity
		if not door:IsDoor() then return end

		pl:SellDoor( door )
	end )

	roleplay.Command( "title", function( pl, args )
		local tr = util.TraceLine{
			start = pl:EyePos(),
			endpos = pl:EyePos() + pl:GetAimVector() * 100,
			filter = pl
		}

		local door = tr.Entity
		if not door:IsDoor() then return end
		if not pl:OwnsDoor( door ) then return end

		local title = table.concat( args, " " )

		door:SetDoorTitle( title )
	end )

	roleplay.Command( "ownable", function( pl, args )
		if not pl:IsAdmin() then return end

		local tr = util.TraceLine{
			start = pl:EyePos(),
			endpos = pl:EyePos() + pl:GetAimVector() * 100,
			filter = pl
		}

		local door = tr.Entity
		if not door:IsDoor() then return end

		door:SetDoorUnownable( false )
	end )

	roleplay.Command( "unownable", function( pl, args )
		if not pl:IsAdmin() then return end

		local tr = util.TraceLine{
			start = pl:EyePos(),
			endpos = pl:EyePos() + pl:GetAimVector() * 100,
			filter = pl
		}

		local door = tr.Entity
		if not door:IsDoor() then return end

		door:SetDoorUnownable( true )
	end )

	roleplay.Command( "doorgroup", function( pl, args )
		if not pl:IsAdmin() then return end

		local tr = util.TraceLine{
			start = pl:EyePos(),
			endpos = pl:EyePos() + pl:GetAimVector() * 100,
			filter = pl
		}

		local door = tr.Entity
		if not door:IsDoor() then return end

		door:SetDoorGroup( table.concat( args, " " ) )
	end )
else
	net.Receive( "DoorOwners", function()
		local door = net.ReadEntity()

		if not IsValid( door ) then return end
		if not door:IsDoor() then return end
		
		door.Owners = {}

		for i = 1, net.ReadUInt( 8 ) do
			door.Owners[ net.ReadEntity() ] = true
		end
	end )

	surface.CreateFont( "DoorTitle", {
		font = "Roboto", 
		size = 28,
		shadow = true
	} )

	surface.CreateFont( "DoorInfo", {
		font = "Roboto", 
		size = 18,
		shadow = true
	} )

	function GM:DrawDoorInfo()
		local tr = util.TraceLine{
			start = LocalPlayer():EyePos(),
			endpos = LocalPlayer():EyePos() + LocalPlayer():GetAimVector() * 256,
			filter = LocalPlayer()
		}

		local door = tr.Entity
		if not door:IsDoor() then return end

		local x, y = ScrW() / 2, ScrH() / 2
		local font = "DoorInfo"

		if not door:GetDoorUnownable() then
			local title = door:GetDoorTitle()
			local owner = door:GetDoorOwner()

			draw.SimpleText( title, "DoorTitle", ScrW() / 2, ScrH() / 2, Color( 100, 200, 255 ),
				TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

			if door:GetDoorGroup() ~= "" then
				draw.SimpleText( "Owned by: " .. door:GetDoorGroup(), font, x, y + 20,
					color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			else
				if IsValid( owner ) then
					draw.SimpleText( "Owned by: " .. owner:Name(), font, x, y + 20,
						color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				else
					draw.SimpleText( "Unowned", font, x, y + 20, Color( 150, 150, 150 ),
						TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				end
			end

			draw.SimpleText( "Press reload with keys out to manage this door", font, x, y + 36,
				Color( 225, 225, 225 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
		else
			draw.SimpleText( "This door is unownable.", font, x, y,
				Color( 255, 50, 50 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
		end
	end
end