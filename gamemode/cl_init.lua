roleplay = {}

include( "shared.lua" )

include( "cl_skin.lua" )
include( "cl_hud.lua" )

include( "vgui/RoleplayVote.lua" )
include( "vgui/RoleplayMenu.lua" )
include( "vgui/RoleplayJobs.lua" )
include( "vgui/RoleplayStore.lua" )
include( "vgui/RoleplayOptions.lua" )

concommand.Add( "rp_menu", function()
	local p = vgui.Create( "RoleplayMenu" )
	p:SetSize( 800, 600 )
	p:Center()
	p:MakePopup()
end )