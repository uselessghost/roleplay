local ChatRange = CreateConVar( "rp_chatrange", 512, FCVAR_ARCHIVE )

local meta = FindMetaTable( "Player" )

function meta:LocalChat( text, range, prefix )
	for _, pl in ipairs( player.GetAll() ) do
		if range > pl:GetPos():Distance( self:GetPos() ) then
			if prefix then
				pl:Message( Color( 100, 200, 255 ), prefix .. " ", color_white, self, ": ", text )				
			else
				pl:Message( self, ": ", text )
			end
		end
	end
end

function meta:Yell( text )
	self:LocalChat( text, ChatRange:GetFloat() * 3, "[Yell]" )
end

function meta:Whisper( text )
	self:LocalChat( text, ChatRange:GetFloat() * 0.25, "[Whisper]" )
end

function meta:OOC( text )
	roleplay.Message( Color( 100, 200, 255 ), "[OOC] ", color_white, self, ": ", text )
end

roleplay.Command( "ooc", function( pl, args ) pl:OOC( table.concat( args, " " ) ) end )
roleplay.Command( "yell", function( pl, args ) pl:Yell( table.concat( args, " " ) ) end )
roleplay.Command( "whisper", function( pl, args ) pl:Whisper( table.concat( args, " " ) ) end )

roleplay.Alias( "/", "ooc" )
roleplay.Alias( "y", "yell" )
roleplay.Alias( "w", "whisper" )
