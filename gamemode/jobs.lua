local Jobs = {}

function roleplay.GetJobs()
	return Jobs
end

function roleplay.GetJob( job )
	return Jobs[ job ]
end

function roleplay.JobExists( job )
	return Jobs[ job ] ~= nil
end

local TeamID = 1
function roleplay.Job( id, opts )
	if not opts.Name then opts.Name = "Invalid Job" end
	if not opts.Description then opts.Description = "This job has no description because it's invalid!" end
	if not opts.Color then opts.Color = Color( 255, 255, 255 ) end
	if not opts.Group then opts.Group = "Everyone" end
	if not opts.Models then opts.Models = { "models/player/kleiner.mdl" } end
	if not opts.Color then opts.Color = Color( 150, 150, 150 ) end
	if not opts.MaxPlayers then opts.MaxPlayers = math.huge end
	if not opts.Salary then opts.Salary = 45 end
	if not opts.Weapons then opts.Weapons = {} end
	if not opts.Ammo then opts.Ammo = {} end
	if not opts.AdminOnly then opts.AdminOnly = false end
	if not opts.Vote then opts.Vote = false end

	opts.Team = TeamID
	team.SetUp( TeamID, opts.Name, opts.Color )
	TeamID = TeamID + 1

	Jobs[ id ] = opts
end

function roleplay.GetJobName( job )
	return Jobs[ job ] and Jobs[ job ].Name or "Invalid Job"
end

local meta = FindMetaTable( "Player" )

function meta:GetJob()
	return self:GetNW2String( "Job" )
end

function meta:GetJobTitle()
	return self:GetNW2String( "JobTitle" )
end

function meta:GetSalary()
	return self:GetNW2Int( "Salary" )
end

if SERVER then
	local DefaultJob = CreateConVar( "rp_defaultjob", "citizen", FCVAR_ARCHIVE )
	local PaydayInterval = CreateConVar( "rp_paydayinterval", 60 * 5, FCVAR_ARCHIVE )

	function GM:PlayerCanSwitchJob( pl, job ) return true end
	function GM:PlayerSwitchedJob( pl, job ) end
	function GM:PlayerCanDemote( demotee, demoter ) return true end
	function GM:PlayerDemoted( demotee, demoter ) end
	function GM:PlayerPayday( pl, salary ) return salary end

	function meta:SetJob( job, dont_notify )
		if not roleplay.JobExists( job ) then return end

		self:SetNW2String( "Job",  job )
		self:SetTeam( roleplay.GetJob( job ).Team )
		self:SetJobTitle( roleplay.GetJob( job ).Name )
		self:SetSalary( roleplay.GetJob( job ).Salary )

		local pos, ang = self:GetPos(), self:EyeAngles()

		self:Spawn()

		self:SetPos( pos )
		self:SetEyeAngles( ang )

		gamemode.Call( "PlayerSwitchedJob", self )

		if not dont_notify then
			roleplay.Notify( string.format( "%s has been made a(n) %s!", self:Name(), roleplay.GetJob( job ).Name ) ) end
	end

	function meta:SetJobTitle( title )
		self:SetNW2String( "JobTitle", title )
	end

	function meta:SetSalary( salary )
		self:SetNW2Int( "Salary", salary )
	end

	function meta:RequestJob( job )
		if not roleplay.JobExists( job ) then return end

		local j = roleplay.GetJob( job )
		if not j then return end

		if j.AdminOnly and not self:IsAdmin() then
			self:NotifyError( "You must be an admin to be a " .. j.Name .. "." ) return end


		if j.ParentJob and self:GetJob() ~= j.ParentJob then
			self:NotifyError( "You need to be a " .. roleplay.GetJob( j.ParentJob ).Name .. " to enter this job." ) return end

		local n = 0
		for _, pl in ipairs( player.GetAll() ) do
			if pl:GetJob() == job then n = n + 1 end
		end

		if j.MaxPlayers <= n then
			self:NotifyError( "This job is full." ) return end

		if j.CanSwitch and not j:CanSwitch( self ) then return end
		if not gamemode.Call( "PlayerCanSwitchJob", pl, job ) then return end

		if j.Vote then 
			local function success()
				self:SetJob( job )
			end

			local function failure()
				roleplay.Notify( string.format( "%s was not made a(n) %s.", self:Name(), j.Name ) )
			end

			roleplay.StartVote( string.format( "%s wants to become a(n) %s.", self:Name(), j.Name ),
				30, success, failure )

			return
		end

		self:SetJob( job )
	end

	function meta:Payday()
		local salary = self:GetSalary()
		salary = gamemode.Call( "PlayerPayday", self, salary )

		if not salary then return end

		self:GiveMoney( salary )
		self:Notify( string.format( "Payday! You received $%s.", string.Comma( salary ) ) )
	end

	function meta:Demote( pl )
		if self:GetJob() == DefaultJob:GetString() then return end

		if not gamemode.Call( "PlayerCanDemote" ) then return end

		local function success()
			roleplay.Notify( string.format( "%s has been demoted.", self:Name() ) )

			gamemode.Call( "PlayerDemoted", self, pl )
			self:SetJob( DefaultJob:GetString() )			
		end

		local function failure()
			roleplay.Notify( string.format( "%s was not demoted.", self:Name() ) )
		end

		roleplay.StartVote( string.format( "%s has been elected for demotion by %s.", self:Name(), pl:Name() ),
			30, success, failure )
	end

	timer.Create( "Payday", PaydayInterval:GetFloat(), 0, function()
		for _, pl in ipairs( player.GetAll() ) do pl:Payday() end
	end )

	roleplay.Command( "job", function( pl, args )
		if args[ 1 ] then pl:RequestJob( args[ 1 ] ) end
	end )

	roleplay.Command( "forcejob", function( pl, args )
		if pl:IsAdmin() and args[ 1 ] then pl:SetJob( args[ 1 ] ) end
	end )

	roleplay.Command( "jobtitle", function( pl, args )
		local title = table.concat( args, " " )
		if title == "" then return end

		pl:SetJobTitle( title )
		pl:Notify( string.format( "You set your job title to \"%s\".", title ) )
	end )

	roleplay.Command( "demote", function( pl, args )
		local target = roleplay.FindPlayer( table.concat( args, " " ) )
		if not IsValid( target ) then return end

		target:Demote( pl )
	end )
end