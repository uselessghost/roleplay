# README #

A basic DarkRP-esque roleplay gamemode for Garry's Mod. Currently supports all basic features that DarkRP does: doors, jobs, etc. 

# What's done

* Chat command system
* Money
* Door ownership + keys
* Job system and some basic jobs
* Voting system (jobs and demotions)
* Ammo, money and weapon entities
* Buying entities + weapons + etc
* All varieties of hooks for custom modifications

# What's not done

* Shipments
* F4 menu
* Hitman system
* Warrants
* Wanted
* Arrests
* Door ram
* Weapon checker
* Lockpicking

# The Maybe List

* Inventory system?