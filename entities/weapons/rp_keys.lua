if SERVER then AddCSLuaFile() end

SWEP.PrintName = "Keys"
SWEP.Category = "Roleplay"
SWEP.Instructions = [[Primary: Lock an owned door
Secondary: Unlock an owned door
Reload: Open door options]]

SWEP.ViewModel = Model( "models/weapons/c_arms.mdl" )
SWEP.WorldModel = ""
SWEP.UseHands = true

SWEP.Slot = 1
SWEP.SlotPos = 1

SWEP.Spawnable = true
SWEP.AdminOnly = false

SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1

SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"
SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1

SWEP.DrawCrosshair = false

function SWEP:Initialize()
	self:SetHoldType( "normal" )
end

function SWEP:GetDoor()
	local tr = util.TraceLine{
		start = self.Owner:EyePos(),
		endpos = self.Owner:EyePos() + self.Owner:GetAimVector() * 100,
		filter = self.Owner
	}

	if not tr.Entity:IsDoor() then return end

	return tr.Entity
end

function SWEP:PrimaryAttack()
	if CLIENT then return end

	self:SetNextPrimaryFire( CurTime() + 0.5 )

	if not gamemode.Call( "PlayerCanLockDoor", self, door ) then return end

	local door = self:GetDoor()
	if not IsValid( door ) then return end

	if not self.Owner:OwnsDoor( door ) then return end

	door:EmitSound( "doors/door_latch3.wav" )
	door:SetDoorLocked( true )

	gamemode.Call( "PlayerLockDoor", self, door )
end

function SWEP:SecondaryAttack()
	if CLIENT then return end

	self:SetNextSecondaryFire( CurTime() + 0.5 )

	if not gamemode.Call( "PlayerCanUnlockDoor", self, door ) then return end

	local door = self:GetDoor()
	if not IsValid( door ) then return end

	if not self.Owner:OwnsDoor( door ) then return end

	door:EmitSound( "doors/default_locked.wav" )
	door:SetDoorLocked( false )

	gamemode.Call( "PlayerUnlockDoor", self, door )
end

function SWEP:Reload()
	if CLIENT then return end
	if not self.Owner:KeyPressed( IN_RELOAD ) then return end

	self:CallOnClient( "ShowDoorOptions" )
end

if CLIENT then
	function SWEP:ShowDoorOptions()
		local door = self:GetDoor()
		if not IsValid( door ) then return end

		local menu = DermaMenu()

		if self.Owner:OwnsDoor( door ) then
			menu:AddOption( "Set Door Title", function()
				Derma_StringRequest( "Title", "Enter the title for this door.", "", function( text )
					RunConsoleCommand( "rp", "title", text )
				end ):SetSkin( "roleplay" )
			end ):SetIcon( "icon16/font.png" )

			if door:GetDoorOwner() == self.Owner then
				menu:AddSpacer()

				local addmenu, entry = menu:AddSubMenu( "Add Co-owner" )
				entry:SetIcon( "icon16/user_add.png" )

				local removemenu, entry = menu:AddSubMenu( "Remove Co-owner" )
				entry:SetIcon( "icon16/user_delete.png" )

				for _, pl in ipairs( player.GetAll() ) do
					if pl ~= door:GetDoorOwner() then
						if pl:OwnsDoor( door ) then
							removemenu:AddOption( pl:Name() )
						else
							addmenu:AddOption( pl:Name() )
						end
					end
				end

				menu:AddSpacer()

				menu:AddOption( "Sell Door", function()
					RunConsoleCommand( "rp", "selldoor" )
				end ):SetIcon( "icon16/money.png" )
			end

		elseif not door:IsOwned() then
			menu:AddOption( "Buy Door", function()
				RunConsoleCommand( "rp", "buydoor" )
			end ):SetIcon( "icon16/door.png" )
		else
			menu:AddOption( "This door is already owned." )
		end

		if self.Owner:IsAdmin() then
			menu:AddSpacer()

			local submenu, entry = menu:AddSubMenu( "Set Door Group" )
			entry:SetIcon( "icon16/group.png" )

			submenu:AddOption( "Remove Door Group", function()
				RunConsoleCommand( "rp", "doorgroup", "" )
			end ):SetIcon( "icon16/group_delete.png" )

			submenu:AddSpacer()

			local groups = {}
			for k, v in pairs( roleplay.GetJobs() ) do
				if v.DoorGroup and not groups[ v.DoorGroup ] then
					submenu:AddOption( v.DoorGroup, function()
						RunConsoleCommand( "rp", "doorgroup", v.DoorGroup )
					end ):SetIcon( "icon16/group_add.png" )

					groups[ v.DoorGroup ] = true
				end
			end

			if door:GetDoorUnownable() then
				menu:AddOption( "Make Ownable", function()
					RunConsoleCommand( "rp", "ownable" )
				end ):SetIcon( "icon16/accept.png" )
			else
				menu:AddOption( "Make Unownable", function()
					RunConsoleCommand( "rp", "unownable" )
				end ):SetIcon( "icon16/delete.png" )
			end
		end

		menu:Open()
		menu:Center()		
	end
end