AddCSLuaFile()

ENT.RenderGroup = RENDERGROUP_TRANSLUCENT 

ENT.Type = "anim"
ENT.PrintName = "Weapon"

function ENT:SetupDataTables()
	self:NetworkVar( "String", 0, "WeaponClass" )
	self:NetworkVar( "Int", 0, "Amount" )
end

if SERVER then
	function ENT:Initialize()
		self:SetModel( "models/Items/item_item_crate.mdl" )

		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetUseType( SIMPLE_USE )

		self:GetPhysicsObject():Wake()

		self:PrecacheGibs()
	end

	function ENT:Use( pl )
		self:EmitSound( "AmmoCrate.Open" )

		local class = self:GetWeaponClass()
		local model = "models/weapons/w_rif_ak47.mdl"

		if weapons.Get( class ) and weapons.Get( class ).WorldModel then
			model = weapons.Get( class ).WorldModel
		end

		local ent = ents.Create( "rp_weapon" )
		ent:SetModel( model )		
		ent:SetPos( self:LocalToWorld( self:GetAngles():Up() * 32 ) )
		ent:SetWeaponClass( class )
		ent:Spawn()

		self:SetAmount( self:GetAmount() - 1 )

		if self:GetAmount() <= 0 then
			self:Break()
		end
	end

	function ENT:Break()
		self:GibBreakClient( self:GetVelocity() )
		self:EmitSound( "Wood.Break" )

		self:Remove()		
	end
else
	function ENT:DrawTranslucent()
		self:DrawModel()

		local ang = self:GetAngles()
		local pos = self:GetPos() + ang:Up() * 13 + ang:Right() * 16.5
		local scale = 0.1

		ang:RotateAroundAxis( ang:Forward(), 90 )

		local name = self:GetWeaponClass()
		local amount = self:GetAmount()

		if weapons.Get( name ) and weapons.Get( name ).PrintName then
			name = weapons.Get( name ).PrintName
		end

		local str = string.format( "%s x%d", name, amount )

		cam.Start3D2D( pos, ang, scale )
			draw.SimpleTextOutlined( str, "DermaLarge", 0, 0, color_white,
				TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, color_black )
		cam.End3D2D()
	end
end