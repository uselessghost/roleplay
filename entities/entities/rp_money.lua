AddCSLuaFile()

ENT.Type = "anim"
ENT.PrintName = "Money"

ENT.Model = Model( "models/props/cs_assault/money.mdl" )

function ENT:SetupDataTables()
	self:NetworkVar( "Int", 0, "Amount" )
end

if SERVER then
	function ENT:Initialize()
		self:SetModel( self.Model )

		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetUseType( SIMPLE_USE )

		self:SetCollisionGroup( COLLISION_GROUP_WEAPON )

		self:GetPhysicsObject():Wake()
	end

	function ENT:Use( pl )
		pl:GiveMoney( self:GetAmount() )
		pl:Notify( string.format( "You found $%s.", string.Comma( self:GetAmount() ) ) )

		self:Remove()
	end
else
	function ENT:Draw()
		self:DrawModel()

		local pos = self:GetPos() + self:GetAngles():Up() * 0.87
		local ang = self:GetAngles()
		local scale = 0.05

		cam.Start3D2D( pos, ang, scale )
			draw.SimpleTextOutlined( "$" .. string.Comma( self:GetAmount() ), "DermaLarge", 0, 0,
				color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, color_black )
		cam.End3D2D()
	end
end
