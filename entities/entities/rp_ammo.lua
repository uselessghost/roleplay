AddCSLuaFile()

ENT.Type = "anim"
ENT.PrintName = "Weapon"

if SERVER then
	AccessorFunc( ENT, "AmmoType", "AmmoType", FORCE_STRING )
	AccessorFunc( ENT, "Amount", "Amount", FORCE_NUMBER )

	function ENT:Initialize()
		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetUseType( SIMPLE_USE )

		self:SetCollisionGroup( COLLISION_GROUP_WEAPON )

		self:GetPhysicsObject():Wake()
	end

	function ENT:Use( pl )
		pl:EmitSound( "Item.Pickup" )
		pl:GiveAmmo( self:GetAmmoAmount(), self:GetAmmoType() )

		self:Remove()
	end
end